# WinFormsChartSamples

#### 介绍
c# chart各种统计表的使用方式示例

#### 软件架构
软件架构说明


#### 安装教程

1. 下载于https://code.msdn.microsoft.com/Windows-Forms-Samples-26bf2a53   
论坛：http://suo.im/4NSDXX  

下载后打开项目会出现编码格式的错误，在VS中点文件-高级保存选项-utf-8即可，该项目中的已经更改好
2. xxxx
3. xxxx

#### 使用说明

1. 演示如下  
![](http://ww1.sinaimg.cn/large/c55aab25gy1g19bh1wkvyg21g80og7wk.gif)  

2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)